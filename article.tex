% gMOSguide.tex % v4.0 released June 2015
\documentclass{gMOS2e}
\usepackage{siunitx}
\usepackage{booktabs}
\usepackage[british]{babel}
\usepackage{amsmath}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{caption}


\usepackage{epstopdf}% To incorporate .eps illustrations using PDFLaTeX, etc. \usepackage{subfigure}% Support for small, `sub' figures and tables
\graphicspath{ {images/} }
\begin{document}

\date{\today\\v1.2}

%\jvol{00} \jnum{00} \jyear{2015} \jmonth{June}
\newcommand{\dlf}{DL\_FIELD }
\newcommand{\dlfns}{DL\_FIELD}
\newcommand{\dlp}{DL\_POLY }
\newcommand{\dlpns}{DL\_POLY}
\newcommand{\dlm}{DL\_MONTE }
\articletype{article}

\title{Molecular Simulation of Hydrogen Storage and Transport in Cellulose}
\author{ 
\name{M.R. Stalker\textsuperscript{a}, J. Grant\textsuperscript{b,c}, C.W. Yong\textsuperscript{d}, L. Ohene-Yeboah\textsuperscript{a},  T.J. Mays\textsuperscript{e} and S.C. Parker\textsuperscript{b}$^{\ast}$\thanks{$^\ast$Corresponding author. Email: S.C.Parker@bath.ac.uk.}}
\affil{\\
\textsuperscript{a}Centre for Sustainable Chemical Technologies, University of Bath, Bath BA2 7AY, United Kingdom;\\
\textsuperscript{b}Department of Chemistry, University of Bath, Bath BA2 7AY, United Kingdom;\\
\textsuperscript{c}Computing Services, University of Bath, Bath BA2 7AY, United Kingdom;\\
\textsuperscript{d}Computation Science and Engineering Department, C.L.R.C. Daresbury Laboratory, Daresbury;\\
\textsuperscript{e}Department of Chemical Engineering, University of Bath, Bath BA2 7AY, United Kingdom}}


\received{xxxx 2018}

\maketitle

\begin{abstract}

In this work we describe a computational workflow to model the sorption and transport of molecular hydrogen
in cellulose frameworks. %Small changes to the unit cell
The work demonstrates the value of \dlp
and \dlm sharing common input formats to enhance the compatibility of
the codes, being supported by \dlfns. Structures generated using cellulose-builder were processed by \dlf to generate input files for \dlp using the OPLS\textunderscore2005 force field. After
relaxation in molecular dynamics, structures were used for
GCMC simulations in \dlm before passing back to \dlp to evaluate
transport properties at different levels of sorption.  While no hydrogen sorption was seen in pure crystalline cellulose, increasing separation between layers did allow sorption.  When slit pores were sufficiently wide, interactions with the cellulose led to the volumetric density of adsorbed hydrogen exceeding vacuum density at accessible partial pressures as well as allowing diffusion through the system.  These model systems can give useful insight into the behaviour of amorphous cellulose in future simulation and experiment.

%\dlp CONFIG and FIELD files using OPLS\_2005 parameters were generated
%using \dlf, and found to reproduce experiment and literature confirming
%the accurate structural classification and forcefield assignment.
%Although crystalline structures did not allow for adsorption, slit pore
%model structures with increasing separation between cellulose layers did allow for
%$H_2$ with adsorption isotherms showing enhanced adsorption due to
%interactions with cellulose when compared with vacuum. Transport
%simulations showed that even when pores allows for adsorption this does
%not necessarily equate to mobility within framework. Our model systems
%while not feasible in practice can give insight into possible behaviour
%of amorphous systems, indicating densities at which hydrogen storage
%might be practicable and directions for future experimental and
%computational efforts. %Further relaxed structures were then generated
%using molecular dynamics %simulations. Monte Carlo simulations performed

%in DL\textunderscore MONTE revealed that crystalline cellulose is not
%sufficiently porous to accommodate hydrogen molecules at sensible
%pressures. However, other cellulose structures namely; slit-pore,
%pinned %and amorphous cellulose did display hydrogen adsorption.The
%transport %behaviour of the hydrogen molecules, investigated using
%Molecular %Dynamics simulation in DL\textunderscore POLY, found that
%diffusion is %driven by porosity within the structures.

 \end{abstract}
 

\begin{keywords} molecular modelling; molecular dynamics (MD); Monte
Carlo (MC); cellulose; hydrogen storage; gas transport ;\dlf; \dlp;
\dlm; cellulose-builder \end{keywords}

\section{Introduction}

\begin{doublespace} Current global energy demands rely upon fossil fuels and their environmental impacts. 
With fossil fuel reserves depleting and global energy demands increasing, the need for more sustainable fuel sources is increasingly recognised. 
As a result, there is growing interest in the development of alternative, renewable energy sources and energy storage.\cite{Parmesan2003}

Hydrogen is a renewable and clean burning fuel which can provide on-demand power, making it a strong candidate to become an integral part of the future energy landscape.\cite{Schuth2011, Eberle2009,
Muller2013, McKeown2007} 
The process of using hydrogen as an energy vector is made up of three distinct parts, namely: production, storage and
utilisation. The storage of hydrogen is regarded as the most challenging aspect of integrating hydrogen into the energy mix, due to the low volumetric density of hydrogen gas. \cite{Germain2009}

Methods used to store hydrogen can be broadly divided into two categories: chemical storage and physical storage (including sorption). \cite{Muller2013} 
Chemical storage is the inclusion of hydrogen (hydrogenation) in chemical compounds. 
At high temperatures hydrogen can then be liberated (dehydrogenation) from the compounds. 
Typical chemical hydrogen storage compounds range from light metal hydrides to complex hydrides.\cite{Mandal2010, Eberle2009, Liu2010, Schlapbach2001}

Physical storage can be enhanced through sorption processes. Sorption
of hydrogen relies upon favourable interaction energies between
molecular hydrogen and porous materials. The high surface area of porous
materials increases the number of interactions between the storage
material and hydrogen. The extent of hydrogen storage depends upon the
specific surface area and pore sizes within the storage material as well
as the strength of the interactions.\cite{Eberle2009} Despite being
numerous, the interactions are weak (typically \textless 10 kJ mol$^{-1}$) and as a result, adequate
storage relies upon low temperatures and high
pressures.\cite{Mandal2010} Candidates for  hydrogen storage materials are typically 
crystalline, including carbonaceous structures, intrinsically
porous frameworks such as metal-organic frameworks (MOFs), covalent
organic frameworks (COFs) and zeolites. \cite{Mandal2010, Muller2013,
Eberle2009, Duren2009, Pukazhselvan2012, Fairen-Jimenez2011}

In addition to crystalline materials, recent research has
investigated the use of porous polymers for hydrogen storage.
Porous polymers provide a lightweight alternative to traditional
metal-based adsorbents, \cite{Trewin2008, McKeown2007, McKeown2006, 
Pukazhselvan2012} since they pack in a way that
results in large voids, which also allows absorptive molecules to permeate their
structures. \cite{Abbott2011} Polymers of intrinsic microporosity (PIMs),
and hyper-cross linked polymers (HCPs) are porous
polymers which have been considered for hydrogen
storage,\cite{McKeown2007, Germain2009, Dawson2012, Muller2013} but the distributions of pore sizes 
and structures complicates analysis. As
a result, a number of researchers have applied computational analysis to
study the properties of PIM. \cite{Larsen2011, Madkour2013, Minelli2013,
Heuchel2008, McKeown2006} and HCP \cite{Wood2007, Abbott2011, Jiang2013}
structures.

%HCP structures exhibit permanent porosity as a result of their
%networked structure produced by cross-linking. Experimental analysis
 %shown that HCP structures are capable of storing over xxxx. ***ref
%ref*** Molecular simulations have been applied to study the surface
%area, pore volume and gas absorption of HCPs. %demonstrated that the
%surface area, pore volume and gas absorption of HCPs increase with
%increased cross linking and identity of the monomer. These predictions
%support with experimental findings that HPCs are a tunable and highly
%porous material. (Wood, Colin D.) (Colina*, Lauren J. Abbott and Coray
%M. - add more references)

%\cite{Dawson2012, Wood2007, McKeown2007, Ismail2015} %
%\cite{Medronho2012, Krishnamachari2011, Perez2010, Rahimi2011,
%Oehme2015}.

Cellulose is the most abundant organic polymer in the biosphere, with
over 100 billion metric tons being synthesised
annually while its
biodegradability makes it more sustainable than
conventional polymers.\cite{Medronho2012, Krishnamachari2011, Perez2010, Rahimi2011, Oehme2015} The polymer is primarily synthesised within plant
cell walls, where it serves as the principal structural polymer forming
characteristic hierarchal structures embedded within a matrix of complex
polysaccharides, including amorphous cellulose.\cite{Brown2000, Matthews2006, Klemm2005}



Individual chains of cellulose comprise$\beta$-(1,4) glycosidic
linkages of glucose residues which assemble
into sheet structures, driven by intermolecular hydrogen
bonds,
which further  assemble into 3-dimensional structures due to van der Waals
interactions.\cite{Oehme2015, Klemm2005, Atalla1990a, Ding2006, Matthews2006, Ciolacu2008, Nishiyama2003} The resultant crystalline structures are referred to as
microfibrils, and exist as a mixture of two allomorphs,
I$\displaystyle\alpha$ and I$\displaystyle\beta$.
\cite{Oehme2015,Ding2006, Nishiyama2003,Cosgrove2005}
Cellulose displays much greater chemical and structural complexity than
conventional polymers and is also insoluble in traditional solvents,
making experimental studies of cellulose challenging. As a result of
such experimental complexities, the simulation of cellulose has been
pursued widely in the literature.

Carbohydrates such as cellulose demonstrate a high density of
functionality and have a number of possible configurations making their
structures complex to simulate. 
Classical modeling has typically been used, but the force
field influences the outcome of
molecular mechanics simulations.\cite{Namboori2008, Damm1997,
Miyamoto2015, Miyamoto2016} As a result, a range of biomolecule-specific
families of force fields such as CHARMM \cite{Guvench2011}, AMBER\cite{Momany2009}, and OPLS\cite{Banks2005} have been developed to model molecules such as carbohydrates.

Molecular modeling has been applied to understand structural
properties including the crystalline networks \cite{Hadden2013,
Bergenstrahle2008a}, mechanical properties
\cite{Liao2012, Kulasinski2014, Tanaka2006, Djahedi2015} and thermal
transitions \cite{Bergenstrahle2007,Chen2012, Chen2004a, Wohlert2012} of
cellulose. Simulations have also been used to investigate the
interactions between cellulose structures and a range of small molecules
including solvents \cite{Bazooyar2013, Glasser2012, Liao2012,
Heiner1998} and model aromatic compounds \cite{Li2015, Mazeau2012,
Woodcock1995, DaSilvaPerez2004}. 

As a result, molecular modeling provides a route to evaluate the feasibility of cellulose as a candidate for hydrogen storage, however, to our knowledge, no research
has previously been reported. % on such studies.
In this article we describe the methods we have used to study the
sorption and transport of hydrogen molecules in various cellulose
structures, before discussing our findings and their implications for
cellulose as a candidate for hydrogen storage.

%Hydrogen shows promise to become a significant energy vector in the
%future energy landscape and finding storage materials to provide
%significant storage of hydrogen is paramount. Polymeric materials show
%potential as hydrogen storage candidates. Cellulose could be sustainable
%feedstock for polymeric hydrogen storage materials. As a result of the
%complexity of the structure of cellulose and the molecular interactions
%which drive hydrogen storage, molecular modeling provides a route to
%investigate the feasibility of cellulose as a hydrogen storage
%candidate.
\input{2_Methods.tex}

\input{3_Results.tex}

\input{Conclusions}
\end{doublespace}
%%References
\renewcommand{\bibname}{References}
%%Changes Bibliography to read References
\clearpage
\bibliographystyle{rsc}
\bibliography{bib}
\end{document}
